import City
import Person
import time



class Hospital(object):

    def __init__(self, city:City, population):
        w,h = city.getSize()
        self.width = w/17.0 #This mimics hosital size that can cover ~.3% of the population
        self.height = h/17.0
        self.bottom = (h - self.height)/2.0
        self.top = (h + self.height)/2.0
        self.left = (w - self.width)/2.0
        self.right = (w + self.width)/2.0
        self.capacity = 1/300 * population #In the US, there's around 1M hospital beds for all 300M population
        self.patients = []
        self.day_one = time.time()


    def getBottom(self):
        return self.bottom

    def getLeft(self):
        return self.left

    def getWidth(self):
        return self.width

    def getTop(self):
        return self.top

    def getRight(self):
        return self.right

    def getHeight(self):
        return self.height

    def admitPatient(self, person: Person):
        self.patients.append(person)
        person.setHealthStatus(Person.HealthStatus.HOSPITALIZED)
        person.quaratine()
        person.moveToHospital()
        person.setTimeOfHospitalization(time.time())

    def releasePatient (self, person: Person):
        self.patients.pop()
        person.unquaratine()

    def getColor(self):
        return 'red' if len(self.patients) >= self.capacity else 'green'

    def isPersonEnclosed(self, person: Person):
        return (person.x <= self.right and person.x>= self.left) and (person.y >= self.bottom and person.y <= self.top)


    def getHospitalCapacity(self):
        return ("Hospitals at {cap}% capacity on day {day}".format(cap=len(self.patients)/self.capacity * 100, day = int(time.time()-self.day_one)))
