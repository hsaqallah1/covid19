import numpy as np
from enum import Enum
import time

import City
import Hospital


class Person(object):

    average_time_show_sickness = 10
    average_time_to_recover_while_hospitalized = 45 #Roughly 4.5 times the time it takes to show sickness

    def __init__(self, city: City, hospital: Hospital, probability_of_infection):
        self.hospital = hospital
        self.city = city
        self.healthStatus = HealthStatus.INCUBATING if (
                    np.random.uniform() <= probability_of_infection) else HealthStatus.VULNERABLE
        if (self.healthStatus == HealthStatus.INCUBATING):
            self.time_of_infection = time.time()
            self.time_of_hospitalization = 0

        w, h = city.getSize()
        self.x = np.random.uniform() * w
        self.y = np.random.uniform() * h
        while (hospital.isPersonEnclosed(self)):
            self.x = np.random.uniform() * w
            self.y = np.random.uniform() * h
        self.setMovement()


    def setMovement(self):
        #Person's move in both directions
        x_range = self.city.getWidth()/100
        y_range = self.city.getHeight()/100
        self.x_delta = np.random.uniform(low=-x_range/2, high=x_range/2)
        self.y_delta = np.random.uniform(low=-y_range/2, high=y_range/2)


    def getHealthStatus(self):
        return self.healthStatus

    def setHealthStatus(self, healthStatus):
        self.healthStatus = healthStatus

    def getColor(self):
        return self.healthStatus.value

    def move(self):
        #SICK People 'might' go to hospital *(25%) probability evey time they move but they're more contageous
        if (self.healthStatus == HealthStatus.SICK and np.random.uniform() <= .25):
            self.hospital.admitPatient(self)

        if (self.x + self.x_delta >= self.city.getWidth() or self.x + self.x_delta <= 0):
            self.x_delta *= -1
        if (self.y + self.y_delta >= self.city.getHeight() or self.y + self.y_delta <= 0):
            self.y_delta *= -1

        self.x += self.x_delta
        self.y += self.y_delta

        #Update health status
        if (self.healthStatus == HealthStatus.INCUBATING and (time.time() - self.time_of_infection) >= Person.average_time_show_sickness):
            self.healthStatus = HealthStatus.SICK

        if (self.healthStatus == HealthStatus.HOSPITALIZED and (time.time() - self.time_of_hospitalization >= Person.average_time_to_recover_while_hospitalized)):
            self.healthStatus = HealthStatus.IMMUNE
            self.hospital.releasePatient(self)



    def isContageous(self):
        return self.healthStatus in (HealthStatus.INCUBATING, HealthStatus.SICK, HealthStatus.HOSPITALIZED)

    def contractDisease(self):
        if (self.healthStatus == HealthStatus.VULNERABLE):
            self.healthStatus = HealthStatus.INCUBATING
            self.time_of_infection = time.time()

    def getTimeOfInfection(self):
        return self.time_of_infection

    def setTimeOfHospitalization(self, time):
        self.time_of_hospitalization = time

    def quaratine(self):
        #i.e. don't move
        self.x_delta = 0
        self.y_delta = 0

    def unquaratine(self):
        self.setMovement()

    def moveToHospital(self):
        self.x = np.random.uniform(low=self.hospital.getLeft(), high=self.hospital.getRight())
        self.y = np.random.uniform(low=self.hospital.getBottom(), high=self.hospital.getTop())



class HealthStatus(str, Enum):
    VULNERABLE = 'green'
    INCUBATING = 'orange'
    SICK = 'red'
    HOSPITALIZED = 'blue'
    IMMUNE = 'purple'
