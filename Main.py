import numpy as np
from matplotlib.lines import Line2D

import Person
import City
import Hospital
from matplotlib import pyplot as plt
from matplotlib import animation






# Create City
city = City.City(20, 20)
probability_of_infection = .005  # Starting with some % of the population in incubation phase

population = 300

#Touching distance
touch_distance = 0.3
# Create Hospital

hospital = Hospital.Hospital(city, population)

# Create population

persons = [Person.Person(city, hospital, probability_of_infection) for i in range(population)]

def detect_collision():
    persons.sort(key=lambda x: x.x, reverse=False)
    x_delta = [(persons[i].x - persons[i - 1].x) for i in range(1, len(persons))]
    x_collision = [i for i in range(len(x_delta)) if x_delta[i] <= touch_distance]  # This list holds the index of the points that are colliding on x axis. Index of 0 means dot[0] and dot[1] are colliding
    xy_collision = [i for i in x_collision if abs(persons[i].y - persons[i + 1].y) <= touch_distance]
    for i in xy_collision:
        if (persons[i].isContageous() or persons[i+1].isContageous()):
            persons[i].contractDisease()
            persons[i+1].contractDisease()
# Draw
grid = plt.figure()
mainax = plt.axes(xlim=(0, city.getWidth()), ylim=(0, city.getHeight()))
plt.gca().axes.xaxis.set_ticklabels([])
plt.gca().axes.yaxis.set_ticklabels([])

scat = mainax.scatter([person.x for person in persons], [person.y for person in persons], s=7,
                      c=[person.getColor() for person in persons], marker = 'o')
hospitalRect = plt.Rectangle((hospital.getLeft(), hospital.getBottom()), hospital.getWidth(), hospital.getHeight(),
                             fill=None, edgecolor=hospital.getColor())
plt.gca().add_patch(hospitalRect)

#Adding legend
legend_elements = [Line2D([0], [0], marker='o', color='w', label=e.name,
                          markerfacecolor=e.value, markersize=7) for e in Person.HealthStatus
                   ]
mainax.legend(handles=legend_elements, loc='lower left')


def animate(f):
    for person in persons:
        person.move()
        detect_collision()
    scat.set_offsets(np.vstack(([person.x for person in persons], [person.y for person in persons])).T)
    scat.set_color([person.getColor() for person in persons])
    plt.title(hospital.getHospitalCapacity())


#    scat.set_color([dot.color for dot in dots])

# Animation
anim = animation.FuncAnimation(grid, animate,frames = 50, interval= 200)


plt.show()
